pluginManagement {
    repositories {
        maven("https://maven.gofancy.wtf/releases")
        gradlePluginPortal()
    }
}

plugins {
    id("wtf.gofancy.convention.locking") version "0.1.0"
    id("wtf.gofancy.convention.buildcache") version "0.1.0"

    id("de.fayard.refreshVersions") version "0.40.2"
}

rootProject.name = "libdatagen"

sequenceOf("base").forEach {
    include(":$it")
    project(":$it").name = "libdatagen-$it"
}
