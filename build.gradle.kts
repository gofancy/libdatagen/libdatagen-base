plugins {
    kotlin("jvm") // required by Dokka
    id("org.jetbrains.dokka")

    id("org.ajoberstar.reckon")
}

allprojects {
    group = "wtf.gofancy.libdatagen"
}

reckon {
//    stages("beta", "rc", "final") for now let's use snapshots, we'll enable this when actually do releases
    snapshots()

    // only needed when using Reckon's tasks, used to calculate the name of the tag to create
    setStageCalc(calcStageFromProp())
    setScopeCalc(calcScopeFromProp())
}

repositories {
    mavenCentral() // required by Dokka
}

tasks {
    withType<Wrapper> {
        gradleVersion = "7.4.2"
        distributionType = Wrapper.DistributionType.ALL
    }
}
