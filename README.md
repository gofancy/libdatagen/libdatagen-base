# LibDatagen

[API Documentation](https://libdatagen.gofancy.wtf)

This project aims to assist in development of software which needs a lot of resource files at compile time. 
Instead of creating those files by hand (we are talking about JSONs and similar) the idea is to specify them 
via a DSL, preferably written in Kotlin Script files which get evaluated by a Gradle task at build time.

## Current State

Currently, the base api gets refined with a better designs to make it as extensible as possible while staying simple
to use.

Once that is finished a small DSL will be created which can be used to specify the resource files.

As a last step a Gradle plugin will be created which integrates this project into a build environment.
