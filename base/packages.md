# Package wtf.gofancy.libdatagen.base.hierarchy

Contains the [DirectoryHierarchy] API. 

A [HierarchyBuilder] needs to be used to create new instances of the hierarchy. The [DirectoryHierarchy.walk] method can
be used to lazily traverse the hierarchy.
