/*
 * This file is part of libdatagen-base, licensed under the MIT License
 *
 * Copyright (c) 2021-2022 Garden of Fancy (dev@gofancy.wtf)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package wtf.gofancy.libdatagen.base.hierarchy

import okio.Path

/**
 * Represents an immutable view of a directory hierarchy.
 *
 * A directory hierarchy is a combination of directories and values, where each directory can store additional
 * subdirectories and values. It closely resembles the structure of a filesystem, although it misses many of its
 * concepts (links for example). The type of the value is determined by [T].
 *
 * To create a new instance of a directory hierarchy a [HierarchyBuilder] needs to be used.
 *
 * ### Implementation Note
 *
 * The design of this data structure was influenced by the popular [Trie](https://en.wikipedia.org/wiki/Trie) structure.
 * Instead of words broken into their characters it represents paths broken into their segments. One can also think of
 * this data structure as a map from paths to values but with an underlying representation optimized for traversal.
 *
 * @constructor
 * Creates a new hierarchy with [root] as root node. The name of the root node must be an empty string.
 *
 * @param root The root node of this hierarchy.
 * @param T The type of value in this hierarchy.
 *
 * @see HierarchyBuilder
 */
class DirectoryHierarchy<T> internal constructor(internal val root: DirectoryNode<T>) {

    /**
     * Retrieves the value specified by [segments] or `null` if non is found in this directory hierarchy.
     *
     * Each entry in [segments] specifies the name of one layer in the hierarchy. One can think of the list as a path
     * which was split at each slash character.
     *
     * @param segments A list of path segments where each segment specifies the name of the node at this hierarchy
     * level.
     * @return The value specified by [segments] or `null` if the path does not exist.
     */
    operator fun get(vararg segments: String): T? {
        if (segments.isEmpty()) return null

        val resourceNodeName = segments.last()
        val directoryNodeNames = segments.dropLast(1)

        var current = root

        for (name in directoryNodeNames) {
            val child = current.children[name]

            if (child == null || child !is DirectoryNode) {
                // The path to the ResourceNode we are searching for does not exist
                return null
            }

            current = child
        }

        return (current.children[resourceNodeName] as? ValueNode)?.value
    }

    /**
     * Retrieves the value located at [path] or `null` if there is no such path in this directory hierarchy.
     *
     * @param path The path to the value. It does not matter if this path is absolute or relative, it will
     * always be interpreted relatively to this hierarchy's root.
     * @return The value located at [path] or `null` if the path does not exist.
     */
    operator fun get(path: Path) = this.get(*path.segments.toTypedArray())
}

internal sealed class HierarchyNode<T>()
internal class DirectoryNode<T>(val children: Map<String, HierarchyNode<T>>) : HierarchyNode<T>()
internal class ValueNode<T>(val value: T) : HierarchyNode<T>()
