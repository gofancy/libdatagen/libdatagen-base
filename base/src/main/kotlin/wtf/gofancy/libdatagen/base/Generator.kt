/*
 * This file is part of libdatagen-base, licensed under the MIT License
 *
 * Copyright (c) 2021-2022 Garden of Fancy (dev@gofancy.wtf)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package wtf.gofancy.libdatagen.base

import okio.Path
import okio.Path.Companion.toPath
import wtf.gofancy.libdatagen.base.Generator.startGenerationProcess
import wtf.gofancy.libdatagen.base.backend.GenerationBackend
import wtf.gofancy.libdatagen.base.cache.Cache
import wtf.gofancy.libdatagen.base.callbacks.GenerationCallbackNotifier
import wtf.gofancy.libdatagen.base.hierarchy.DirectoryHierarchy
import wtf.gofancy.libdatagen.base.hierarchy.walk
import wtf.gofancy.libdatagen.base.resource.ByteEncodingException
import wtf.gofancy.libdatagen.base.resource.Resource

/**
 * The Generator is the main entry point for starting a "generator".
 *
 * In fact, there is no generator object which holds state and can be passed around. To start a generation process call
 * [startGenerationProcess], the methods parameters will guide you.
 */
object Generator {

    private fun handleDirectory(rootPath: Path, dirPath: Path, backend: GenerationBackend) {
        val path = rootPath / dirPath

        try {
            backend.writeDirectory(path)
        } catch (e: Exception) {
            // TODO actually we do not skip the directory, we still try to write the children because we are lazy..
            notifier.notifyDirectorySkip(path.parent(), path.name, e)
        }

        notifier.notifyDirectoryStart(path.parent(), path.name)
    }

    private fun handleResource(rootPath: Path, resPath: Path, res: Resource, backend: GenerationBackend, cache: Cache) {
        val path = rootPath / resPath

        val bytes = try {
            res.bytes()
        } catch (e: ByteEncodingException) {
            notifier.notifyResourceSkip(path.parent(), path.name, e)
            return
        }

        if (cache.query(path, bytes)) {
            notifier.notifyResourceCacheHit(path.parent(), path.name)
            return
        }

        notifier.notifyResourceStart(path.parent(), path.name)

        try {
            backend.writeResource(path, bytes)
        } catch (e: Exception) {
            notifier.notifyResourceFail(path.parent(), path.name, e)
            return
        }

        cache.update(path, bytes)

        notifier.notifyResourceSuccess(path.parent(), path.name)
    }

    private fun Path.parent() = this.parent ?: "/".toPath()

    /**
     * The notifier for pushing generation events.
     */
    val notifier = GenerationCallbackNotifier()

    /**
     * Launches a new generation process and waits for it to exit.
     *
     * @param rootPath Specifies the parent of the [hierarchy]. If this is set to `/generated` and the
     * [hierarchy]'s root name is `files` and contains a value with the name `values.json` its final location would be
     * `/generated/files/values.json`.
     * @param hierarchy Contains the complete directory hierarchy to generate.
     * @param backend This gets used to perform the final write operations (of directories and resources) and therefor
     * provides an abstraction layer over the storage medium.
     * @param cache Gets used to avoid generating files which are already present on the storage medium.
     */
    fun startGenerationProcess(
        rootPath: Path,
        hierarchy: DirectoryHierarchy<Resource>,
        backend: GenerationBackend,
        cache: Cache,
    ) {
        notifier.notifyGenerationStart(rootPath)

        val (directories, resources) = hierarchy.walk()

        directories.forEach {
            handleDirectory(rootPath, it.path, backend)
        }

        resources.forEach {
            handleResource(rootPath, it.path, it.value, backend, cache)
        }

        cache.persist()

        notifier.notifyGenerationFinish(rootPath)
    }
}
