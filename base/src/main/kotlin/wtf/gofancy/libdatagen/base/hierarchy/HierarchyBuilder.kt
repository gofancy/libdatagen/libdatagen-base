/*
 * This file is part of libdatagen-base, licensed under the MIT License
 *
 * Copyright (c) 2021-2022 Garden of Fancy (dev@gofancy.wtf)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package wtf.gofancy.libdatagen.base.hierarchy

/**
 * Builder class for [DirectoryHierarchy].
 *
 * - [directory] adds a subdirectory to the current hierarchy by relaying on a new builder instance to create its
 * structure.
 * - [value] adds an object of type [T] as direct child of the current hierarchy.
 * - [build] creates a new [DirectoryHierarchy] from the current configuration
 *
 * **Naming Restrictions**
 *
 * The strings given to the above-mentioned methods are interpreted as identifiers for either a directory or a value
 * and are therefor part of a path. To avoid problems when interpreting the structure following restrictions are put on
 * those strings:
 *
 * - must not contain any `/` (slash) or `\` (backslash)
 * - must not contain only `.` (dots)
 *
 * If a string fails to adhere to those restrictions an IllegalArgumentException gets thrown.
 *
 * @param T The type of the value in this hierarchy.
 *
 * @see DirectoryHierarchy
 */
class HierarchyBuilder<T> {

    private val children = mutableMapOf<String, HierarchyNode<T>>()

    /**
     * Adds a new subdirectory to the current hierarchy.
     *
     * If there already exists a subdirectory with this name, it gets replaced.
     *
     * If [builder] defines an empty directory it does not get added.
     *
     * @param name The name of this subdirectory. Needs to adhere the restrictions as specified in [HierarchyBuilder].
     * @param builder The builder for the subdirectory. It can safely be modified again afterwards, possibly even
     * building its own hierarchy.
     * @return This builder to allow for method chaining.
     */
    fun directory(name: String, builder: HierarchyBuilder<T>): HierarchyBuilder<T> {
        name.verifyNamingRestrictions()

        builder.children.let {
            if (it.isNotEmpty()) {
                this.children[name] = DirectoryNode(it.toMap())
            }
        }

        return this
    }

    /**
     * Adds a new value to the current hierarchy.
     *
     * If there already exists a value with this name, it gets replaced.
     *
     * @param name The name of this value. Needs to adhere the restrictions as specified in [HierarchyBuilder].
     * @param value The actual value.
     * @return This builder to allow for method chaining.
     */
    fun value(name: String, value: T): HierarchyBuilder<T> {
        name.verifyNamingRestrictions()

        this.children[name] = ValueNode(value)
        return this
    }

    /**
     * Creates a new [DirectoryHierarchy] from the current configuration.
     *
     * This method can be invoked multiple times. Care must be taken for the values in those hierarchies as those will
     * be the same instance everywhere. Directories on the other hand are independent of each other.
     *
     * @return The newly created directory hierarchy.
     */
    fun build(): DirectoryHierarchy<T> {
        return DirectoryHierarchy(DirectoryNode(this.children.toMap()))
    }

    /**
     * Checks this string for invalid characters as described in [HierarchyBuilder].
     * @see HierarchyBuilder
     */
    @Throws(IllegalArgumentException::class)
    private fun String.verifyNamingRestrictions() {
        val error = when {
            this.contains('/') -> "'/' character"
            this.contains('\\') -> "'\\' character"
            this.toCharArray().distinct().let {
                it.size == 1 && it.first() == '.'
            } -> "only '.' characters"
            else -> null
        }

        if (error != null) {
            throw IllegalArgumentException("The identifier '$this' is invalid because it contains $error.")
        }
    }
}
