/*
 * This file is part of libdatagen-base, licensed under the MIT License
 *
 * Copyright (c) 2021-2022 Garden of Fancy (dev@gofancy.wtf)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package wtf.gofancy.libdatagen.base.cache

import okio.ByteString
import okio.Path

/**
 * A [Generator][wtf.gofancy.libdatagen.base.Generator] utilizes a cache to avoid generating a
 * [Resource][wtf.gofancy.libdatagen.base.resource.Resource] which already exists and can therefore safely be skipped.
 *
 * Because a generator will often run standalone (meaning not during runtime and rather once, before the actual
 * application runs) the cache's content cannot be stored in-memory. This would defeat the purpose of the cache
 * since each time the generator runs it would be empty. Instead, the cache utilize a [CacheStorage] to store its
 * content persistently.
 *
 * The cache uses a write-back cache strategy. This means that when the cache gets [updated][update] the new entry does
 * not get written through to the persistent layer. Instead, the changes are temporarily only reflected in-memory and
 * get written back later, when [persist] gets called.
 *
 * The cache does **not** sync itself with the current content of the
 * [GenerationBackend][wtf.gofancy.libdatagen.base.backend.GenerationBackend]. This means that if resource files get
 * deleted by hand in between generation processes, that change does not get reflected in the cache.
 *
 * ### Implementation Note
 *
 * [ByteString.sha256] gets used to transform the given bytes into a hash which can be persisted and compared more
 * easily.
 */
class Cache(private val storage: CacheStorage) {

    private val cache by lazy { storage.read().toMutableMap() }

    /**
     * Looks up [path] in this cache and compares the retrieved value with [bytes].
     *
     * If the values are equal, true is returned, and it can be assumed that the resource at [path] does not need to be
     * generated. Otherwise, false is returned and the resource needs to be generated.
     *
     * The cache will be accessed read-only in this operation. This means if the values don't match up the cache will
     * not be adapted to the new value directly. To update the cache use [update].
     *
     * ### API Note
     *
     * The reason why the cache needs to be updated manually is because of the possibility that a resource fails to
     * generate. If that happens and the cache got updated directly it would contain an invalid value, which would
     * need to be reverted afterwards. Since this would complicate the cache api and implementations this approach was
     * not taken.
     *
     * @param path the path of the resource which should be queried
     * @param bytes the bytes which are planned to be generated, will be compared with the bytes currently stored
     *              for `path`
     * @return true if the resource at `path` can be skipped by the generator (because it already exists with the same
     *         content), false if it needs to be generated
     */
    fun query(path: Path, bytes: ByteString): Boolean {
        return this.cache[path] == bytes.sha256()
    }

    /**
     * Updates the value stored for [path] with [bytes].
     *
     * ### API Note
     *
     * Call this method only **after** the resource was generated. Otherwise, the cache could end up with invalid entries
     * which can not be removed.
     *
     * @param path the path of the resource which should be updated
     * @param bytes the bytes which should be put as new value for `path`
     */
    fun update(path: Path, bytes: ByteString) {
        this.cache[path] = bytes.sha256()
    }

    /**
     * Persists the current in-memory content of the cache back to persistent storage.
     *
     * ### API Note
     *
     * This method should be called at least once at the end of a generators' execution.
     */
    fun persist() {
        storage.write(this.cache)
    }
}
