/*
 * This file is part of libdatagen-base, licensed under the MIT License
 *
 * Copyright (c) 2021-2022 Garden of Fancy (dev@gofancy.wtf)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package wtf.gofancy.libdatagen.base.callbacks

import okio.Path

class GenerationCallbackNotifier {

    private val callbacks = mutableSetOf<GenerationCallback>()

    fun addCallback(callback: GenerationCallback) {
        this.callbacks.add(callback)
    }

    fun notifyGenerationStart(at: Path) {
        this.forEachCallback {
            it.onGenerationStart(at)
        }
    }

    fun notifyGenerationFinish(at: Path) {
        this.forEachCallback {
            it.onGenerationFinish(at)
        }
    }

    fun notifyDirectoryStart(parent: Path, name: String) {
        this.forEachCallback {
            it.onDirectoryStart(parent, name)
        }
    }

    fun notifyDirectorySkip(parent: Path, name: String, cause: Throwable) {
        this.forEachCallback {
            it.onDirectorySkip(parent, name, cause)
        }
    }

    fun notifyDirectoryFinish(parent: Path, name: String) {
        this.forEachCallback {
            it.onDirectoryFinish(parent, name)
        }
    }

    fun notifyResourceStart(directory: Path, name: String) {
        this.forEachCallback {
            it.onResourceStart(directory, name)
        }
    }

    fun notifyResourceCacheHit(directory: Path, name: String) {
        this.forEachCallback {
            it.onResourceCacheHit(directory, name)
        }
    }

    fun notifyResourceSuccess(directory: Path, name: String) {
        this.forEachCallback {
            it.onResourceSuccess(directory, name)
        }
    }

    fun notifyResourceSkip(directory: Path, name: String, cause: Throwable) {
        this.forEachCallback {
            it.onResourceSkip(directory, name, cause)
        }
    }

    fun notifyResourceFail(directory: Path, name: String, cause: Throwable) {
        this.forEachCallback {
            it.onResourceFail(directory, name, cause)
        }
    }

    private inline fun forEachCallback(action: (GenerationCallback) -> Unit) {
        this.callbacks.forEach(action)
    }
}
