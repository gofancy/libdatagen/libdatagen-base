/*
 * This file is part of libdatagen-base, licensed under the MIT License
 *
 * Copyright (c) 2021-2022 Garden of Fancy (dev@gofancy.wtf)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package wtf.gofancy.libdatagen.base.cache

import okio.ByteString
import okio.ByteString.Companion.encodeUtf8
import okio.FileSystem
import okio.Path
import okio.Path.Companion.toPath

/**
 * A simple cache storage which stores entries at a given [Path] in a given [FileSystem].
 *
 * ### Implementation Note
 *
 * Entries get stored one per line in the format `<path>:<hash>`.
 *
 * @constructor Creates a new cache storage which will store entries at [location] in [fs].
 * @param location The location of the file which should be used as persistent cache storage.
 * @param fs The filesystem where the file should be created. This parameter exists mainly to allow an in-memory
 * filesystem to be used during testing.
 */
class FileStorage(
    private val location: Path,
    private val fs: FileSystem = FileSystem.SYSTEM,
) : CacheStorage {

    override fun read(): Map<Path, ByteString> {
        return this.fs.read(this.location) {
            this.readUtf8().lineSequence()
                .map { it.substringBefore(':').toPath() to it.substringAfter(':').encodeUtf8() }
                .toMap()
        }
    }

    override fun write(cache: Map<Path, ByteString>) {
        this.fs.write(this.location) {
            this.writeUtf8(cache.asSequence().map { it.key.toString() + ":" + it.value.utf8() }.joinToString("\n"))
        }
    }
}
