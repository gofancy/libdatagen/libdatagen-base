/*
 * This file is part of libdatagen-base, licensed under the MIT License
 *
 * Copyright (c) 2021-2022 Garden of Fancy (dev@gofancy.wtf)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package wtf.gofancy.libdatagen.base.resource

import kotlinx.serialization.*
import kotlinx.serialization.modules.SerializersModule
import okio.ByteString
import wtf.gofancy.libdatagen.util.encodeToByteString
import wtf.gofancy.libdatagen.util.serializer
import wtf.gofancy.libdatagen.util.verifyType
import kotlin.reflect.KType
import kotlin.reflect.typeOf

/**
 * A resource designed to work with [kotlinx.serialization](https://github.com/kotlin/kotlinx.serialization).
 *
 * During byte conversion the specified [KType] gets used to find a suitable [KSerializer]. Using that serializer and
 * the specified [SerialFormat] the content then gets encoded into bytes.
 *
 * @constructor
 * Constructs a new resource with the specified filename and the given content encoded as bytes. To encode the object
 * the [KType] instance gets used to find a [KSerializer] and the format gets used to define the final layout of the
 * content.
 *
 * As such, it must be possible to find a fitting serializer for [T] in the given formats [SerializersModule] by using
 * the provided type. The simplest way to do so is by using [typeOf] and marking the class [T] as
 * [Serializable].
 *
 * The format must either be a [StringFormat] or a [BinaryFormat] since it is unknown how to convert other formats outputs
 * into bytes. Besides that, no restrictions on the used format are made as long as it can encode the content.
 *
 * For more information about the process of serialization and possible other ways to define a serializer for a specific
 * class please see kotlinx.serializations [official guide](https://github.com/Kotlin/kotlinx.serialization/blob/master/docs/serialization-guide.md).
 *
 * @param [content] Gets encoded into bytes which then define the generated files final content.
 * @param [type] Gets used to find the [KSerializer] to serialize content.
 * @param [format] Gets used to encode content into bytes.
 * @param [T] The type of the content.
 */
class SerializationResource<T : Any>(
    content: T,
    type: KType,
    format: SerialFormat,
) : Resource {

    private val lazyBytes by lazy {
        verifyType(type, content)
        format.encodeToByteString(format.serializer(type), content)
    }

    override fun bytes(): ByteString = this.lazyBytes
}

/**
 * A convenient helper function to construct a new [SerializationResource] which uses [typeOf] to determine the type of
 * [T].
 *
 * For more information about the restrictions made on the arguments please see the documentation for the
 * SerializationResource constructor.
 *
 * @param [content] Gets encoded into bytes which then define the generated files final content.
 * @param [format] Gets used to encode content into bytes.
 * @param [T] The type of the content.
 *
 * @see SerializationResource
 */
@Suppress("FUNCTION_NAME")
inline fun <reified T : Any> SerializationResource(
    content: T,
    format: SerialFormat
) = SerializationResource(content, typeOf<T>(), format)
