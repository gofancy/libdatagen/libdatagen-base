/*
 * This file is part of libdatagen-base, licensed under the MIT License
 *
 * Copyright (c) 2021-2022 Garden of Fancy (dev@gofancy.wtf)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package wtf.gofancy.libdatagen.base.backend

import okio.ByteString
import okio.FileSystem
import okio.IOException
import okio.Path

/**
 * A generator backend which integrates with okio's [FileSystem] to write resources and directories.
 *
 * @constructor
 * Constructs a new backend which will use the specified filesystem as its backend. By default [FileSystem.SYSTEM] is
 * used.
 * @param fs The filesystem where the resources and directories should be written to.
 */
class FileSystemBackend(
    private val fs: FileSystem = FileSystem.SYSTEM
) : GenerationBackend {

    override fun writeDirectory(at: Path) {
        val metadata = this.fs.metadataOrNull(at)

        // a null metadata means that there is nothing at this path currently
        if (metadata != null && !metadata.isDirectory) {
            throw IOException("expected '$at' to be a directory or non-existent")
        }

        this.fs.createDirectories(at)
    }

    override fun writeResource(at: Path, bytes: ByteString) {
        val metadata = this.fs.metadataOrNull(at)

        // a null metadata means that there is nothing at this path currently
        if (metadata != null && !metadata.isRegularFile) {
            throw IOException("expected '$at' to be a regular file or non-existent")
        }

        this.fs.write(at) {
            write(bytes)
        }
    }
}
