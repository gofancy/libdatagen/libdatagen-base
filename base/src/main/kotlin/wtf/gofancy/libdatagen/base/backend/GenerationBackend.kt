/*
 * This file is part of libdatagen-base, licensed under the MIT License
 *
 * Copyright (c) 2021-2022 Garden of Fancy (dev@gofancy.wtf)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package wtf.gofancy.libdatagen.base.backend

import okio.ByteString
import okio.IOException
import okio.Path
import wtf.gofancy.libdatagen.base.Generator

/**
 * Strategy interface for writing directories and files to a storage medium. Used in [Generator.startGenerationProcess]
 * to store the created resources.
 */
interface GenerationBackend {

    /**
     * Gets invoked to write a directory at [path][at].
     *
     * Implementations are expected to be able to handle cases where the [path][at]'s parents do not already exist. In
     * practise this means that implementations should be prepared to create all the directory's parents as well.
     *
     * Furthermore, it is also quite possible that the directory itself does already exist, so the creation should not
     * be forced. It may also happen that another filesystem node (like a file or a symlink) already exists on the
     * storage medium at [path][at], in such cases an exception should be raised to indicate a failed write operation.
     *
     * @param at The path where the directory should be created.
     * @throws IOException If the write operation fails for any reason.
     */
    @Throws(IOException::class)
    fun writeDirectory(at: Path)

    /**
     * Gets invoked to write a resource (so basically a file) at [path][at].
     *
     * Implementation should expect the file's parent directories to exist.
     *
     * If there already exists another file at [path][at] it can be overridden but if the storage medium has a different
     * node at the path (such as a directory or a symlink) the implementation should fail with an exception.
     *
     * @param at The path where the file should be created.
     * @param bytes The content of the newly created file.
     * @throws IOException If the write operation fails for any reason.
     */
    @Throws(IOException::class)
    fun writeResource(at: Path, bytes: ByteString)
}
