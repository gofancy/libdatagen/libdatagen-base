/*
 * This file is part of libdatagen-base, licensed under the MIT License
 *
 * Copyright (c) 2021-2022 Garden of Fancy (dev@gofancy.wtf)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package wtf.gofancy.libdatagen.base.callbacks

import okio.Path
import wtf.gofancy.log.api.Flog

object LoggingCallback : GenerationCallback {

    private val l = Flog.of("LibDatagen", "Generation")

    override fun onGenerationStart(at: Path) {
        l.i("%s: STARTED", at)
    }

    override fun onGenerationFinish(at: Path) {
        l.i("%s: FINISHED", at)
    }

    override fun onDirectoryStart(parent: Path, name: String) {
        l.i("%s: directory started", parent / name)
    }

    override fun onDirectorySkip(parent: Path, name: String, cause: Throwable) {
        l.wx("%s: directory skipped", cause, parent / name)
    }

    override fun onDirectoryFinish(parent: Path, name: String) {
        l.i("%s: directory finished successfully", parent / name)
    }

    override fun onResourceStart(directory: Path, name: String) {
        l.i("%s: resource started", directory / name)
    }

    override fun onResourceCacheHit(directory: Path, name: String) {
        l.i("%s: resource cache hit (skipped)", directory / name)
    }

    override fun onResourceSuccess(directory: Path, name: String) {
        l.i("%s: resource finished successfully", directory / name)
    }

    override fun onResourceSkip(directory: Path, name: String, cause: Throwable) {
        l.wx("%s: resource skipped", cause, directory / name)
    }

    override fun onResourceFail(directory: Path, name: String, cause: Throwable) {
        l.wx("%s: resource failed", cause, directory / name)
    }
}
