/*
 * This file is part of libdatagen-base, licensed under the MIT License
 *
 * Copyright (c) 2021-2022 Garden of Fancy (dev@gofancy.wtf)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package wtf.gofancy.libdatagen.base.callbacks

import okio.Path

interface GenerationCallback {

    /**
     * Start of generation.
     * @param at path of root directory
     */
    fun onGenerationStart(at: Path) {}

    /**
     * End of generation.
     * @param at path of root directory
     */
    fun onGenerationFinish(at: Path) {}

    /**
     * When the generator starts with the generation of a directory and all its contents. If the generator instead
     * decides to not generate a directory [onDirectorySkip] gets called instead.
     * @param parent the path to the parent directory
     * @param name the name of the directory itself
     * @see onDirectorySkip
     */
    fun onDirectoryStart(parent: Path, name: String) {}

    /**
     * When the generator was unable to create a directory (for example because there was already a file at the path
     * of the directory). Being unable to create the directory means all subdirectories and resources in it will be
     * skipped as well, though no callbacks get invoked for them.
     *
     * This callback is an alternative to the [onDirectoryStart] function, there will never be both called for the same
     * directory. The generator either creates the directory, or it does not.
     * @param parent the path to the parent directory
     * @param name the name of the directory itself
     * @param cause an exception describing why the directory gets skipped
     */
    fun onDirectorySkip(parent: Path, name: String, cause: Throwable) {}

    /**
     * When the generator finishes with the generation of a directory.
     * @param parent the path to the parent directory
     * @param name the name of the directory itself
     */
    fun onDirectoryFinish(parent: Path, name: String) {}

    /**
     * When the generator starts to generate a new resource. If the resource instead gets found in the cache (if a cache
     * exists) [onResourceCacheHit] gets called instead.
     * @param directory the path of the directory where the resource gets generated in
     * @param name the file name of the resource
     */
    fun onResourceStart(directory: Path, name: String) {}

    /**
     * When the generator finds the resource in its cache (note that not all generators necessarily implement a cache),
     * and therefore decides to skip it.
     *
     * This callback is an alternative to the [onResourceStart] function, there will never be both called for the same
     * resource.
     * @param directory the path of the directory where the resource gets generated in
     * @param name the file name of the resource
     */
    fun onResourceCacheHit(directory: Path, name: String) {}

    /**
     * When the generator successfully finishes with the generation of a new resource.
     * @param directory the path of the directory where the resource gets generated in
     * @param name the file name of the resource
     */
    fun onResourceSuccess(directory: Path, name: String) {}

    /**
     * When some error (usually during byte encoding) prevents the resource from being generated.
     *
     * @param directory the path of the directory where the resource gets generated in
     * @param name the file name of the resource
     * @param cause an exception describing why the generation of this resource gets skipped
     */
    fun onResourceSkip(directory: Path, name: String, cause: Throwable) {}

    /**
     * When there is an error during actual generating of the resource. The cause could be everything, from a simple
     * generic [java.io.IOException] over network transfer problems to database errors, depending solely on the
     * implementation details of the used generator.
     * @param directory the path of the directory where the resource gets generated in
     * @param name the file name of the resource
     * @param cause an exception describing why the generation of this resource failed
     */
    fun onResourceFail(directory: Path, name: String, cause: Throwable) {}
}
