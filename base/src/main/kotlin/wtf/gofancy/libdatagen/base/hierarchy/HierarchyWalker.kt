/*
 * This file is part of libdatagen-base, licensed under the MIT License
 *
 * Copyright (c) 2021-2022 Garden of Fancy (dev@gofancy.wtf)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package wtf.gofancy.libdatagen.base.hierarchy

import okio.Path
import okio.Path.Companion.toPath

/**
 * Holds data about a directory in a [DirectoryHierarchy] found by iterating through it.
 *
 * @property path The path of the directory, relative to the hierarchy's root. It is important to keep in mind that the
 * walker traverses an abstract representation of a directory hierarchy which is not necessarily based on any physical
 * storage device. Therefor, it might be possible that this path already exists on the device but is a file or a
 * symlink rather than a directory.
 */
data class DirectoryData(val path: Path)

/**
 * Holds data about a value in a [DirectoryHierarchy].
 *
 * @property path The path of the value, relative to the hierarchy's root. It is important to keep in mind that the
 * walker traverses an abstract representation of a directory hierarchy which is not necessarily based on any physical
 * storage device. Therefor, it might be possible that this path already exists on the device but is a directory or a
 * symlink rather than a file.
 * @property value The value stored at in the hierarchy at [path].
 */
data class ValueData<T>(val path: Path, val value: T)

/**
 * Holds the two different sequences returned by [walk][DirectoryHierarchy.walk].
 *
 * @property directories A sequence returning all directories in a [DirectoryHierarchy] by traversing it lazily. The
 * sequence can be iterated multiple times, but is not thread safe.
 * @property values A sequence returning all values in a [DirectoryHierarchy] by traversing it lazily. The sequence can
 * be iterated multiple times, but is not thread safe.
 *
 * @see DirectoryHierarchy.walk
 */
data class WalkerSequences<T>(val directories: Sequence<DirectoryData>, val values: Sequence<ValueData<T>>)

/**
 * Walks through this [DirectoryHierarchy] and returns two sequences describing all directories and all values in this
 * structure.
 *
 * The returned sequences are encapsulated in a [WalkerSequences], which can be destructured.
 *
 * It is important to understand that the returned sequences share the same traversal state and utilize a cache to store
 * the already discovered directories/values. This means, no matter how often the different sequences will be iterated,
 * the hierarchy itself will only ever be traversed once. Furthermore, the whole traversal algorithm works lazily.
 *
 * The returned sequences can be iterated multiple times, but are **not** thread safe. Iterating through the same
 * sequence or both sequences at the same time in multiple threads leads to undefined behavior.
 *
 * @receiver The hierarchy which gets traversed.
 * @return A wrapper around the two created sequences.
 *
 * @see WalkerSequences
 * @see Sequence
 * @see DirectoryData
 * @see ValueData
 */
fun <T> DirectoryHierarchy<T>.walk(): WalkerSequences<T> {
    val dirCache = ArrayDeque<DirectoryData>()
    val valCache = ArrayDeque<ValueData<T>>()

    val walker = Walker(dirCache, valCache, this.root)

    val directorySequence = sequence {
        walk(walker, dirCache)
    }
    val valueSequence = sequence {
        walk(walker, valCache)
    }

    return WalkerSequences(directorySequence, valueSequence)
}

/**
 * Utilizes the given [Walker] to yield values to this sequence scope. Before a call to the walker's
 * [advance][Walker.advance] method the cache gets checked for already existing entries.
 *
 * @receiver The receiver of [yield][SequenceScope.yield].
 * @param T The type of the walker.
 * @param E The type to yield from the cache.
 */
private suspend fun <T, E> SequenceScope<E>.walk(walker: Walker<T>, cache: ArrayDeque<E>) {
    var i = 0

    while (true) {
        while (i < cache.size) {
            yield(cache[i++])
        }

        if (walker.isFinished) {
            break
        } else {
            walker.advance()
        }
    }
}

/**
 * Represents the state of the traversal algorithm. Each call to [advance] processes the children of the next directory
 * node in the queue, starting at the root node.
 *
 * All discovered directories and values will be added to the caches which can then be used by the sequences to avoid
 * iterating a hierarchy more than once.
 */
private class Walker<T>(
    private val dirCache: ArrayDeque<DirectoryData>,
    private val valCache: ArrayDeque<ValueData<T>>,
    root: DirectoryNode<T>,
) {

    private val todo = ArrayDeque<Pair<Path, DirectoryNode<T>>>()

    /**
     * `true` if the walker processed *all* nodes in a hierarchy. Therefor if all nodes are already added to the caches.
     */
    val isFinished get() = todo.isEmpty()

    init {
        todo.addLast("".toPath() to root)
    }

    /**
     * Processes the next directory node in the queue and adds the children to the cache. After a call to this method
     * the cache can be checked again for new values.
     */
    fun advance() {
        if (this.isFinished) return

        val (path, current) = todo.removeFirst()

        for ((name, node) in current.children) {
            val nodePath = path / name

            when (node) {
                is DirectoryNode<T> -> this.todo.addLast(nodePath to node)
                is ValueNode<T> -> this.valCache.addLast(ValueData(nodePath, node.value))
            }
        }

        this.dirCache.addLast(DirectoryData(path))
    }
}
