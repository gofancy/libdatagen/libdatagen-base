/*
 * This file is part of libdatagen-base, licensed under the MIT License
 *
 * Copyright (c) 2021-2022 Garden of Fancy (dev@gofancy.wtf)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package wtf.gofancy.libdatagen.util

import kotlinx.serialization.*
import okio.ByteString
import okio.ByteString.Companion.encodeUtf8
import okio.ByteString.Companion.toByteString
import wtf.gofancy.libdatagen.base.resource.ByteEncodingException
import kotlin.reflect.KClass
import kotlin.reflect.KType

internal fun verifyType(type: KType, instance: Any) {
    val clazz = type.classifier as? KClass<*>
        ?: throw ByteEncodingException("Expected type to represent a class but got '${type.classifier.qualifier}'.")

    if (!clazz.isInstance(instance)) {
        throw ByteEncodingException("Expected instance to be a subtype of '${type.classifier.qualifier}' but got '${instance.qualifier}'.")
    }
}

internal fun SerialFormat.serializer(type: KType): KSerializer<Any?> = try {
    this.serializersModule.serializer(type)
} catch (e: SerializationException) {
    throw ByteEncodingException("Expected to find a serializer for '${type.classifier.qualifier}' in the formats serializers module.", e)
}

internal fun SerialFormat.encodeToByteString(serializer: KSerializer<Any?>, value: Any): ByteString = try {
    this.encodeToByteStringFun.invoke(serializer, value)
} catch (e: SerializationException) {
    throw ByteEncodingException("Expected '${value.qualifier}' to be encodable with '${this.qualifier}'.", e)
}

private val SerialFormat.encodeToByteStringFun: (KSerializer<Any?>, value: Any) -> ByteString
    get() = when (this) {
        is BinaryFormat -> { serializer, value ->
            this.encodeToByteArray(serializer, value).toByteString()
        }
        is StringFormat -> { serializer, value ->
            this.encodeToString(serializer, value).encodeUtf8()
        }
        else -> throw ByteEncodingException("Expected format to be either of type binary or string, but got '${this.qualifier}'.")
    }
