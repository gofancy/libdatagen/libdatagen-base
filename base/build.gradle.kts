import org.jetbrains.dokka.gradle.DokkaTaskPartial
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import java.net.URL
import java.util.*

plugins {
    id("wtf.gofancy.convention.publishing")

    kotlin("jvm")
    kotlin("plugin.serialization")

    id("org.jetbrains.dokka")
    id("org.cadixdev.licenser")
}

license {
    header(project.file("NOTICE").let { if (it.exists()) it else project.rootProject.file("NOTICE") })

    include("**/*.kt")

    ext["year"] = "2021-${Calendar.getInstance().get(Calendar.YEAR)}"
    ext["name"] = "Garden of Fancy"
    ext["email"] = "dev@gofancy.wtf"
    ext["app"] = "libdatagen-${project.name}"
}

publishing {
    publications {
        val default by creating(MavenPublication::class) {
            from(project.components.getByName("java"))
        }
    }
}

repositories {
    mavenCentral()
    maven {
        name = "Garden of Fancy / FancyLog"
        url = uri("https://gitlab.com/api/v4/projects/29005781/packages/maven")
    }
}

dependencies {
    implementation(platform(kotlin("bom", version = "_")))
    implementation(Kotlin.stdlib.jdk8)

    // -- okio --
    api(platform("com.squareup.okio:okio-bom:_"))
    api(Square.okio)
    testImplementation(group = "com.squareup.okio", name = "okio-fakefilesystem")

    // -- kotlinx.serialization --
    api(KotlinX.serialization.core)
    testImplementation(KotlinX.serialization.json)

    // -- gofancy log --
    implementation(group = "wtf.gofancy.log", name = "api", version = "_")
    testImplementation(group = "wtf.gofancy.log", name = "golfer", version = "_")
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_1_8.toString()
    }
}

tasks.withType<DokkaTaskPartial> {
    suppressInheritedMembers.set(true)

    dokkaSourceSets.configureEach {
        reportUndocumented.set(true)

        includes.from("packages.md")

        externalDocumentationLink {
            url.set(URL("https://kotlin.github.io/kotlinx.serialization/"))
            packageListUrl.set(URL("https://kotlin.github.io/kotlinx.serialization/package-list"))
        }
        externalDocumentationLink {
            url.set(URL("https://square.github.io/okio/3.x/okio/"))
            packageListUrl.set(URL("https://square.github.io/okio/3.x/okio/okio/package-list"))
        }
        sourceLink {
            localDirectory.set(project.file("src/main/kotlin"))
            remoteUrl.set(uri("https://gitlab.com/gofancy/libdatagen/libdatagen-base/-/tree/main/base/src/main/kotlin/").toURL())
            remoteLineSuffix.set("#L")
        }
    }
}
